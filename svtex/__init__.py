#!/usr/bin/env python

#========================================================================================================
import sys, os, subprocess
import re

from sys import platform


class VTF:
	extension = '.vtf'
	def __init__(self, relativePath):
		self.name = os.path.splitext( os.path.basename( relativePath ) )[0]
		parts = self.name.rsplit('_')

		if len(parts) == 2:
			self.suffix = '_' + parts[1]
			self.prefix = ''
			self.name = parts[0]
		elif len(parts) >= 3:
			self.prefix = parts[0] + '_'
			#print(self.prefix)
			self.suffix = '_' + parts[len(parts) - 1]
			self.name = '_'.join(parts[1:len(parts) - 1])
			#print(self.name)
		else:
			self.suffix = ''
			self.prefix = ''

		self.relativePath = relativePath

	def compile( self, targetVtex, gameDirectory, materialsrcDirectory):
		if platform == "windows":
			subprocess.call([targetVtex, '-quiet', self.get_raw_path_from(materialsrcDirectory), '-game', gameDirectory ])
		elif platform == "linux":
			custom_env = os.environ.copy()
			custom_env["WINEDEBUG"] = '-all'
			cmd = ["wine", targetVtex, '-nopause', self.get_raw_path_from(materialsrcDirectory), '-game', as_winepath(gameDirectory)]
			subprocess.call(cmd, env=custom_env)

	def get_raw_path_from( self, directory ):
		return os.path.join( directory, self.relativePath )

	def get_vmt_path( self ):
		return os.path.splitext( self.relativePath )[0]

	def get_compiled_path( self, gameDirectory ):
		return os.path.join( gameDirectory, 'materials', self.get_vmt_path() + '.vtf' )

	def get_suffix( self ):
		return self.suffix

	def get_prefix( self ):
		return self.prefix

class VMT:
	def __init__( self, basevtf, bumpvtf, envvtf ):
		if basevtf is None:
			self.baseRelPath = ""
		else:
			self.baseRelPath = basevtf.get_vmt_path()

		if bumpvtf is None:
			self.bumpRelPath = ''
		else:
			self.bumpRelPath = bumpvtf.get_vmt_path()

		if envvtf is None:
			self.envRelPath = 'env_cubemap'
		else:
			self.envRelPath = envvtf.get_vmt_path()

	def save( self, templateFilePath, vmtFilePath ):
		if not templateFilePath:
			return

		with open( templateFilePath, 'r') as templateFile:
			templateData = templateFile.read()
			if self.baseRelPath:
				templateData = templateData.replace('<basetexture>', self.baseRelPath)
			else:
				templateData = templateData.replace('$basetexture', '//$basetexture')

			if self.bumpRelPath:
				templateData = templateData.replace('<bumpmap>', self.bumpRelPath)
			else:
				templateData = templateData.replace('$bumpmap', '//$bumpmap' )

			templateData = templateData.replace('<envmap>', self.envRelPath)

			#backup older vmt file if it exists
			if os.path.isfile( vmtFilePath ):
				with open(vmtFilePath, 'r') as originalFile:
					originalData = originalFile.read()
					with open(vmtFilePath + '.bak', 'w') as backupFile:
						backupFile.write( originalData )

			#write new vmt file
			with open( vmtFilePath, 'w') as vmtFile:
				vmtFile.write(templateData)


#==============================================================================

def as_winepath(directory):
	"""
	Utility for linux compatibility layer which converts a unix path to the
	appropriate wine directory hierarchy. This is used specifically for the
	game directory argument of vtex...
	"""
	cmd = "WINEDEBUG=-all winepath -w '" + directory + "'"
	newDirectory = subprocess.check_output(cmd, shell=True)
	newDirectory = newDirectory.replace(b"\n", b"")
	return newDirectory

def compile(targetVtex="./", gameDirectory="./game", materialsrcDirectory="./materialsrc", materialTemplate={}, materialTemplateDirectory="./materialsrc"):
	if not handle_errors(targetVtex, gameDirectory, materialsrcDirectory, materialTemplate):
		print("^^^ ERROR ^^^")
		return

	vtfFiles = make_vtf_files(targetVtex, gameDirectory, materialsrcDirectory)
	make_vmt_files( vtfFiles, gameDirectory, materialTemplate, materialTemplateDirectory)

def make_vtf_files(targetVtex, gameDirectory, materialsrcDirectory):
	vtfFiles = []
	for root, dirs, files in os.walk(materialsrcDirectory):
		for file in files:
			if file.endswith(".tga") or file.endswith(".psd"):
				relPath = os.path.relpath( os.path.join(root, file), materialsrcDirectory )
				vtfFiles.append( VTF( relPath) )

	for vtf in vtfFiles:
		vtf.compile(targetVtex, gameDirectory, materialsrcDirectory)

	return vtfFiles

def make_vmt_files( vtfFiles, gameDirectory, materialTemplate, materialTemplateDirectory ):
	companionTable = {}
	#find normal and cube maps
	for vtf in vtfFiles:
		if vtf.name not in companionTable:
			companionTable[vtf.name] = {}

		if vtf.suffix == "_n" or vtf.suffix == "_normal":
			companionTable[vtf.name]["normal"] = vtf
		elif vtf.suffix == "_env" or vtf.suffix == "_cubemap":
			companionTable[vtf.name]["cubemap"] = vtf
		else:
			companionTable[vtf.name][vtf.suffix[1:]] = vtf

	for vtf in vtfFiles:
		if vtf.suffix != "_base" and vtf.suffix != "_diffuse":
			continue

		#Get place to save the vmt by getting path of vtf and replacing the extension name
		vmtPath = re.sub( r'\.(vtf)$', '.vmt', vtf.get_compiled_path(gameDirectory))

		normalvtf = None
		if "normal" in companionTable[vtf.name]:
			normalvtf = companionTable[vtf.name]["normal"]

		cubemapvtf = None
		if "cubemap" in companionTable[vtf.name]:
			cubemapvtf = companionTable[vtf.name]["cubemap"]

		print('Using template : ' + vmtPath + ' - for material: ' + vtf.name)
		vmt = VMT( vtf, normalvtf, cubemapvtf )
		vmt.save( os.path.join(materialTemplateDirectory,
		   materialTemplate.get(vtf.prefix[:-1], materialTemplate['default'])),
					    vmtPath )

def handle_errors(targetVtex, gameDirectory, materialsrcDirectory, materialTemplate):
	""" This is where we will check for any errors --
	will eventually use argument parameters instead of globals sometime...

	Returns true only if there are no errors found.
	"""
	if materialsrcDirectory.find( "materialsrc") == -1 :
		print("Raw, uncompiled textures must be" + \
		      "within a folder called 'materialsrc'. " )
		print("e.g. >> '../materialsrc/texture.tga'" + \
		      "or '../materialsrc/myassets/texture2.tga' ")
		return False

	if not os.path.isdir(materialsrcDirectory):
		print_directory_not_found("materialsrcDirectory", materialsrcDirectory)
		return False

	if not os.path.isdir(gameDirectory) or \
	   not os.path.isdir(gameDirectory + '/materials'):
		print_directory_not_found( "gameDirectory", gameDirectory )
		print('Or you may not be pointing to the folder' + \
		      'that contains gameinfo file.')
		return False

	if not os.path.isfile(targetVtex) or targetVtex.find('vtex') == -1:
		print('targetVtex is either incorrect or unspecified.')
		print('Directory should look something like the following:')
		print('directory_to_steam/steamapps/common/[GameName]/bin/vtex.exe')
		return False

	if len(materialTemplate.keys()) == 0:
		return False

	return True

def print_directory_not_found( variableName, directoryName ):
	print("Specified directory for '" + variableName + "' (" + \
	       directoryName + ") is invalid or does not exist.")
	print("Make sure that the directory exists or check for errors")
