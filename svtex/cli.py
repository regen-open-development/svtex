import svtex

import sys, os, subprocess
import re
from sys import platform

from tkinter import filedialog
from tkinter import *
import yaml
import argparse


def str2bool(v):
    """
    argparse for utility for boolean flags from:
    https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    """
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def main():
    Tk().withdraw() # Spawn + hide root tkinter window.
    config_path = os.path.expanduser("~") + "/.config/svtex.yaml"
    parser = argparse.ArgumentParser(description="Simple VTex Batch Compilation")
    parser.add_argument("--reconfigure", type=str2bool, nargs='?', const=True, default=False, help="Flag to trigger a reconfiguration.")
    parser.add_argument('--config', default=os.path.expanduser("~") + "/.config/svtex.yaml", help="Custom location for configuration file.")
    args = parser.parse_args()

    try:
        configuration_file = open(args.config, "r")
    except:
        configuration_file = open(args.config, "w+")
    configuration = yaml.load(configuration_file) or {}

    def inquire_file(title = "Select File",
                     types = [("Windows Executables","*.exe")]):
        return filedialog.askopenfilename(initialdir = "/",title = title, \
                                           filetypes = types)

    def inquire_directory(title = "Select Directory"):
        return filedialog.askdirectory(initialdir = "/", title = title)

    targetVtex = configuration["vtex_path"] if \
                 "vtex_path" in configuration and not args.reconfigure \
                 else inquire_file(title="Select VTex Executable")

    gameDirectory = configuration["game_directory"] if \
                    "game_directory" in configuration and not args.reconfigure \
                    else inquire_directory(title = "Select Game Directory")

    materialsrcDirectory = configuration["materialsrc_directory"] if \
                           "materialsrc_directory" in configuration and not args.reconfigure \
                           else inquire_directory( title = "Select MaterialSRC Directory" )
    materialTemplate = {}
    materialTemplateDirectory = configuration["material_templates_directory"] if \
                                "material_templates_directory" in configuration and not args.reconfigure\
                                else inquire_directory(title = "Select Material Template Directory")

    #If there's no specified materialsrc folder, try the one located in working directory.
    if not materialsrcDirectory:
        print( "No material src directory specified, using one located in directory: ")
        print( "        " + os.path.dirname(__file__))
        materialsrcDirectory = os.path.dirname(__file__)
    else:
        if not os.path.isabs(materialsrcDirectory):
            os.path.join( os.path.dirname(__file__), materialsrcDirectory )

    print( "Compiling files from: " + materialsrcDirectory)

    if not materialTemplateDirectory:
        print( "No material template directory specified, using python file directory: ")
        materialTemplateDirectory = os.path.dirname(__file__)

    for file in os.listdir(materialTemplateDirectory):
        if file.endswith('.vmtmp'):
            materialTemplate[os.path.splitext(os.path.basename(file))[0]] = file

    svtex.compile(targetVtex=targetVtex, gameDirectory=gameDirectory, materialsrcDirectory=materialsrcDirectory, materialTemplate=materialTemplate, materialTemplateDirectory=materialTemplateDirectory)

    with open(args.config, "w+") as outfile:
        yaml.dump(configuration, outfile)
