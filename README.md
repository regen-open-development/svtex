## About:
Simple VTEX is a Source Engine command line utility used for batch compiling VTF files and
making the desired accompanying VMT files. I made this for my own personal
Valve Hammer use on my Windows / Linux machines. It comes with a Linux compatibility layer
(with the expectation that you have a copy of the game installed via Valve's
proton compatibility). It also works on windows, of course.

##Installation:
###Windows:
Python 3 is required to run this project. In order to install this tool, you should make
sure that python is properly exposed to your Windows environment path. Those details can be
found on the python language documentation. Once that's done, and you have a copy of a source
engine game installed on your computer, you can download this project (either as zip or git clone).
Navigate to the folder containing the setup.py file, right click on the file browser and click
`Open Command Window Here` and type in the following:

```
pip install .
```

You can now run the `svtex` command from your command line application of choice!

###Linux:
Install and configure Python 3. You must also have wine installed (I recommend wine-staging)
and a copy of Source SDK and CSGO installed via Steam Proton (aka Steam Play). With that done,
you can follow the same instructions as Windows above (Clone the repository, run `pip install .`
in the root of the repository.)

##Configuration
This tool uses a YAML configuration to store all the details necessary to run the
script.

Example Yaml:
```
vtex_path: [PATH/TO/VTEX.exe] #FILL THIS IN
#"""
#File location of the tool needed to 'make' textures (vtfs).
#This should be a vtex.exe file stored in a specific
#source engine game's bin folder.

#e.g.
#"steamapps/common/Team Fortress 2/bin/vtex.exe"

#VTEX is a proprietary tool made by Valve Software and only works on Windows.

#This value cannot be left as empty and must be specified for each game.
#This will determine where and how your vtfs are made.

#Directory should use either forward slashes (/) or double backslashes (\\).
$Windows default (\) will cause errors and should be avoided.
#"""


game_directory: [PATH/TO/GAME_DIRECTORY] #FILL THIS IN!
#"""
#Game directory which contains the 'materials' folder along with other game content
#directories. Should also have a gameinfo.txt.

#e.g.
#"steamapps/common/Team Fortress 2/tf/"

#Directory should use either forward slashes (/) or double backslashes (\\). Windows default (\) will cause
#errors in a few edgecases and should be avoided.
#"""



materialsrc_directory: "../../materialsrc"
#"""
#The directory where you store all of your .psd or .tga files.
#An equivalent folder will be made for every folder within materialsrc.
#For example, if you want your material to be stored in:
#'../Team Fortress 2/tf/materials/example/',

#Your materialsrc directory layout should look like:
#"[yourdirectory/materialsrc]/example/texturename.tga"

#If this is left as an empty string, it will check for a materialsrc
#folder next to the script.
#"""

material_templates_directory: ""
#"""
#This should be a directory containing .vmtmp files. These are a basic template used to make a material file.
#An example of what a key value pair might look like in a template file:

#	$basetexture <basetexture>

# The '<basetexture>' will be automatically replaced with an actual texture file when the material is being created.

# For a more clear example, check out 'default.vmtmp' which comes with this script.
# """
```

For multi-game configuration, you can create multiple configuration files and hotswap them
by changing the command line arguments...

`svtex --config ./svtex-tf2.yaml`
