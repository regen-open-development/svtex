from setuptools import setup

setup(name='svtex',
      version='0.1',
      description='Simple VTEX CLI',
      url='https://gitlab.com/regen-open-development/svtex',
      author='Eoin O\'Neill (Studio Regen)',
      author_email='eoinoneill1991@gmail.com',
      license='MIT',
      packages=['svtex'],
      entry_points={
            'console_scripts': [
                'svtex=svtex.cli:main'
            ],
        },
      install_requires=[
          'PyYAML'
      ],
      zip_safe=False)
